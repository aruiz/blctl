// lib.rs
//
// Copyright 2019 Alberto Ruiz <aruiz@gnome.org>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
// SPDX-License-Identifier: MPL-2.0

pub mod bls;
pub mod cmdline;
pub mod cshim;
